<html>
    <head>
        <!--  <link rel="stylesheet" href="CSS/Sources/Home.css" type="text/css" /> -->
        <script type="text/javascript" src="./js/jquery.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    </head>
    <body>
        <?php
        include_once 'clases/sessionFunc.php';
        include_once 'clases/loginFunc.php';
        include_once 'clases/db_Connect.php';
        

        sec_session_start();

        $log = new loginFunc();

        if ($log->login_check($mysqli) == true) {
            include_once 'interface/header.php';
            include_once 'interface/content.php';
            require_once 'interface/footer.php';
        } else {
            header('Location: login.php?error=1');
        }
        ?>
    </body>
</html>

