<?php
include_once '../clases/project.php';
include_once '../clases/db_Connect.php';

$msg = "";
$proj = new Project();
if (isset($_POST['data'])) {
    if ($_POST['data'] == 'active_project') {
        $var = $proj->get_active_project_page_data_from_db($mysqli);
        echo json_encode($var);
    }

    if ($_POST['data'] == 'past_project') {
        $msg = $proj->get_past_project_page_data_from_db($mysqli);
        echo json_encode($msg);
    }
}
?>
   