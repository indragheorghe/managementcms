<?php

include_once '../clases/project.php';
include_once '../clases/db_Connect.php';
include_once '../clases/sessionFunc.php';

sec_session_start();

$msg = "";
$proj = new Project();
$user_id = $_SESSION['user_id'];
$var = $proj->get_active_project_page_data_from_db($mysqli);

if (isset($_POST['data'])) {
    if ($_POST['data'] == 'active_project') {
        echo json_encode($var);
    }

    if ($_POST['data'] == 'past_project') {
        $msg = $proj->get_past_project_page_data_from_db($mysqli);
        echo json_encode($msg);
    }
    if ($_POST['data'] == 'my_projects') {
        $msg = $proj->get_user_projects_from_db($mysqli,$user_id);
        echo json_encode($msg);
    }
     if ($_POST['data'] == 'current_projects') {
        $msg = $proj->get_user_current_projects_from_db($mysqli, $user_id);
        echo json_encode($msg);
    }
}
?>
   