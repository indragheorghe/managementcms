<?php
include_once '../clases/sessionFunc.php';
include_once '../clases/db_Connect.php';
include_once '../clases/user.php';

sec_session_start();

$msg = "";
$user_id = $_SESSION['user_id'];
$usr = new User();
$usr->get_data_from_db($mysqli, $user_id);

if (isset($_POST['data'])) {
    if ($_POST['data'] == 'user') {
        echo $usr->get_first_name();
    }
    $arraymsg = array();
    if ($_POST['data'] == 'all_user_data') {
        array_push($arraymsg, $usr->get_first_name());
        array_push($arraymsg, $usr->get_last_name());
        array_push($arraymsg, $usr->get_email());
        array_push($arraymsg, $usr->get_username());
        array_push($arraymsg, $usr->get_cellphone());
        array_push($arraymsg, $usr->get_birthday());
        array_push($arraymsg, $usr->get_category_id());
        echo json_encode($arraymsg);
    }
}
?>
  