<?php
include_once 'clases/user.php';
include_once 'clases/db_Connect.php';


class Requests {

    public function __construct() {
        ;
    }

    function complete_user_data($mysqli, $usr) {
        $user_id = $_SESSION['user_id'];
        $stmt = $mysqli->prepare("SELECT first_name, last_name, birthday, email, username, cellphone, category_id FROM user WHERE id = ? LIMIT 1");
        $stmt->bind_param('i', $user_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($first_name1, $last_name1, $birthday1, $email1, $username1, $cellphone1, $category_id1);
        $stmt->fetch();

       
        $usr->set_data($last_name1, $first_name1, $birthday1, $email1, $username1, $cellphone1, $category_id1);
        
        return $usr;
    }
    

}
?>
