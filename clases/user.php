<?php

include_once 'db_Connect.php';

class User {

    private $last_name;
    private $first_name;
    private $birthday;
    private $email;
    private $username;
    private $cellphone;
    private $category_id;
    private $id;

    function __construct() {
        ;
    }
// no db connection
    function set_data($id, $last_name, $first_name, $birthday, $email, $username, $cellphone, $category_id) {
        $this->last_name = $last_name;
        $this->first_name = $first_name;
        $this->birthday = $birthday;
        $this->email = $email;
        $this->username = $username;
        $this->cellphone = $cellphone;
        $this->id = $id;
        $this->category_id = $category_id;
    }

    function get_last_name() {
        return $this->last_name;
    }

    function get_first_name() {
        return $this->first_name;
    }

    function get_birthday() {
        return $this->birthday;
    }

    function get_email() {
        return $this->email;
    }

    function get_username() {
        return $this->username;
    }

    function get_cellphone() {
        return $this->cellphone;
    }

    function get_category_id() {
        return $this->category_id;
    }

    function set_last_name($last_name) {
        $this->last_name = $last_name;
    }

    function set_first_name($first_name) {
        $this->first_name = $first_name;
    }

    function set_birthday($birth) {
        $this->birthday = $birth;
    }

    function set_email($email) {
        $this->email = $email;
    }

    function set_username($username) {
        $this->username = $username;
    }

    function set_cellphone($cellphone) {
        $this->cellphone = $cellphone;
    }

    function set_category_id($category_id) {
        $this->category_id = $category_id;
    }
    
    // with db connection
    function get_data_from_db($mysqli, $user_id)
    {
        $stmt = $mysqli->prepare("SELECT first_name, last_name, birthday, email, username, cellphone, category_id FROM user WHERE id = ? LIMIT 1");
        $stmt->bind_param('i', $user_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($first_name1, $last_name1, $birthday1, $email1, $username1, $cellphone1, $category_id1);
        $stmt->fetch();
        
        $this->set_data($user_id, $last_name1, $first_name1, $birthday1, $email1, $username1, $cellphone1, $category_id1);
    }
    
    function set_first_name_db($first_name)
    {
        $querry= $mysqli->prepare("Update person set first_name= ? where id= ?");
        $querry->bind_param('si', $first_name, $this->id);
        
    }
    // de facut si pt restul dupa ce il testam

}

?>