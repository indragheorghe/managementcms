<?php

include_once 'db_Connect.php';

// clasa nu este inca completa. mai trebuie modificata conform modificarilor bazei de date
class Project {

    private $name;
    private $description;
    private $tagline;
    private $user_admin;
    //private $admin_type;
    private $created_by_user;
    private $project_type;
    private $start_date;
    private $end_date;
    private $creation_date;
    private $last_signup_day;

    public function __construct() {
        ;
    }

    function set_data($name, $description, $tagline, $start_date, $end_date) {
        $this->name = $name;
        $this->description = $description;
        $this->tagline = $tagline;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    function get_active_project_page_data_from_db($mysqli) {
        $now = date('Y-m-d');
        $stmt = $mysqli->query("SELECT id, name, description, tagline, start_date, end_date FROM project where end_date >'" . $now . "' order by start_date desc");
        $proj_array = array();
        for ($row_no = $stmt->num_rows - 1; $row_no >= 0; $row_no--) {
            $stmt->data_seek($row_no);
            $row = $stmt->fetch_assoc();
            array_push($proj_array, $row);
        }

        return $proj_array;
    }

    function get_past_project_page_data_from_db($mysqli) {
        $now = date('Y-m-d');
        $stmt = $mysqli->query("SELECT id, name, description, tagline, start_date, end_date FROM project where end_date <'" . $now . "' order by start_date desc");
        $proj_array = array();
        for ($row_no = $stmt->num_rows - 1; $row_no >= 0; $row_no--) {
            $stmt->data_seek($row_no);
            $row = $stmt->fetch_assoc();
            array_push($proj_array, $row);
        }

        return $proj_array;
    }

    function get_user_projects_from_db($mysqli, $user_id) {
        $now = date('Y-m-d');
        $stmt = $mysqli->query("SELECT id, name, description, tagline, start_date, end_date FROM project where created_by=".$user_id." order by start_date desc");
        $proj_array = array();
        for ($row_no = $stmt->num_rows - 1; $row_no >= 0; $row_no--) {
            $stmt->data_seek($row_no);
            $row = $stmt->fetch_assoc();
            array_push($proj_array, $row);
        }
        return $proj_array;
    }
    function get_user_adminitered_projects_from_db($mysqli, $user_id) {
        $now = date('Y-m-d');
        $stmt = $mysqli->query("SELECT id, name, description, tagline, start_date, end_date FROM project where admin=".$user_id." order by start_date desc");
        $proj_array = array();
        for ($row_no = $stmt->num_rows - 1; $row_no >= 0; $row_no--) {
            $stmt->data_seek($row_no);
            $row = $stmt->fetch_assoc();
            array_push($proj_array, $row);
        }
        return $proj_array;
    }
    function get_user_current_projects_from_db($mysqli, $user_id) {
        //proiectele la care participa userul acum
        // momentan interogarea e gresita!!!!!!!
        $stmt = $mysqli->query("SELECT id, name, description, tagline, start_date, end_date FROM project where admin=".$user_id." order by start_date desc");
        $proj_array = array();
        for ($row_no = $stmt->num_rows - 1; $row_no >= 0; $row_no--) {
            $stmt->data_seek($row_no);
            $row = $stmt->fetch_assoc();
            array_push($proj_array, $row);
        }
        return $proj_array;
    }
     function get_project_types($mysqli) {
        $stmt = $mysqli->query("SELECT * from project_type");
        $proj_array = array();
        for ($row_no = $stmt->num_rows - 1; $row_no >= 0; $row_no--) {
            $stmt->data_seek($row_no);
            $row = $stmt->fetch_assoc();
            array_push($proj_array, $row);
        }
        return $proj_array;
    }

}
?>