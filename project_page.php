
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include_once 'clases/sessionFunc.php';
        include_once 'clases/loginFunc.php';
        include_once 'clases/db_Connect.php';


        sec_session_start();

        $log = new loginFunc();

        if ($log->login_check($mysqli) == true) {
            include_once 'interface/header.php';
           include_once 'interface/project_page_content.php';
            require_once 'interface/footer.php';
        } else {
            header('Location: login.php?error=1');
        }
        ?>
    </body>
</html>
