<?php
include 'clases/db_Connect.php';
include 'clases/loginFunc.php';
include 'clases/sessionFunc.php';

$log= new loginFunc();
sec_session_start(); // Our custom secure way of starting a php session. 


 
if(isset($_POST['email'], $_POST['p'])) { 
   $email = $_POST['email'];
   $password = $_POST['p']; // The hashed password.
   if($log->login($email, $password, $mysqli) == true) {
         header('Location: index.php');
   } else {
      // Login failed
        header('Location: login.php?error=1');
   }
} else { 
   // The correct POST variables were not sent to this page.
   echo 'Invalid Request';
}
?>