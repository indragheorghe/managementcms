<script type="text/javascript" src="./js/calendar.js"></script>
<script type="text/javascript" src="./js/view.js"></script>
<div>
    <label for="project_name">Project Name</label>
    <input type="text" id="project_name" class="text_input"/>

    <label for="tag_line">Project Tagline</label>
    <input type="text" id="tag_line" class="text_input"/>

    <label for="description_textarea">Description</label>
    <textarea rows="10" cols="40" id="description_textarea"></textarea>

    // drop down manu pentru tipuri de proiect din javascript

    <div id="cal1" class="calendar_proj">
        <label class="description" for="element_5">Start Date </label>
        <span>
            <input id="element_5_1" name="element_5_1" class="element text" size="2" maxlength="2" value="" type="text"> /
            <label for="element_5_1">MM</label>
        </span>
        <span>
            <input id="element_5_2" name="element_5_2" class="element text" size="2" maxlength="2" value="" type="text"> /
            <label for="element_5_2">DD</label>
        </span>
        <span>
            <input id="element_5_3" name="element_5_3" class="element text" size="4" maxlength="4" value="" type="text">
            <label for="element_5_3">YYYY</label>
        </span>

        <span id="calendar_5">
            <img id="cal_img_5" class="datepicker" src="./CSS/images/calendar.gif" alt="Pick a date.">	
        </span>
        <script type="text/javascript">
            Calendar.setup({
                inputField	 : "element_5_3",
                baseField        : "element_5",
                displayArea      : "calendar_5",
                button		 : "cal_img_5",
                ifFormat	 : "%B %e, %Y",
                onSelect	 : selectDate
            });
        </script>
    </div>

    <div id="cal2" class="calendar_proj">
        <label class="description" for="element_4">End Date </label>
        <span>
            <input id="element_4_1" name="element_4_1" class="element text" size="2" maxlength="2" value="" type="text"> /
            <label for="element_5_1">MM</label>
        </span>
        <span>
            <input id="element_4_2" name="element_4_2" class="element text" size="2" maxlength="2" value="" type="text"> /
            <label for="element_5_2">DD</label>
        </span>
        <span>
            <input id="element_4_3" name="element_4_3" class="element text" size="4" maxlength="4" value="" type="text">
            <label for="element_5_3">YYYY</label>
        </span>

        <span id="calendar_4">
            <img id="cal_img_4" class="datepicker" src="./CSS/images/calendar.gif" alt="Pick a date.">	
        </span>
        <script type="text/javascript">
            Calendar.setup({
                inputField	 : "element_4_3",
                baseField        : "element_4",
                displayArea      : "calendar_4",
                button		 : "cal_img_4",
                ifFormat	 : "%B %e, %Y",
                onSelect	 : selectDate
            });
        </script>
    </div>


    <div id="cal3" class="calendar_proj">

        <label class="description" for="element_3">Last Application Date </label>
        <span>
            <input id="element_3_1" name="element_3_1" class="element text" size="2" maxlength="2" value="" type="text"> /
            <label for="element_5_1">MM</label>
        </span>
        <span>
            <input id="element_3_2" name="element_3_2" class="element text" size="2" maxlength="2" value="" type="text"> /
            <label for="element_5_2">DD</label>
        </span>
        <span>
            <input id="element_3_3" name="element_3_3" class="element text" size="4" maxlength="4" value="" type="text">
            <label for="element_5_3">YYYY</label>
        </span>

        <span id="calendar_3">
            <img id="cal_img_3" class="datepicker" src="./CSS/images/calendar.gif" alt="Pick a date.">	
        </span>
        <script type="text/javascript">
            Calendar.setup({
                inputField	 : "element_3_3",
                baseField        : "element_3",
                displayArea      : "calendar_3",
                button		 : "cal_img_3",
                ifFormat	 : "%B %e, %Y",
                onSelect	 : selectDate
            });
        </script>
    </div>

    <label for="select_privacy_menu">Select project privacy</label>
    <select>
        <option>Private project</option>
        <option>Public Project</option>
    </select>
</div>
