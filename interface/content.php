<?php

include_once 'clases/sessionFunc.php';
include_once 'clases/loginFunc.php';
include_once 'clases/db_Connect.php';

// sec_session_start();
$page = "home";
if (isset($_GET['page']))
    $page = $_GET['page'];


if ($log->login_check($mysqli) == true) {

   // $request = new Requests();
   // $usr = new User();
   // $usr = $request->complete_user_data($mysqli, $usr);

    if ($page == 'profile')
        include_once './interface/profile.php';
    if ($page == 'my_prj')
        include_once './interface/my_projects.php';
    if ($page == 'act_prj')
        include_once './interface/active_projects.php';
    if ($page == 'past_prj')
        include_once './interface/past_projects.php';
    if ($page == 'cur_prj')
        include_once './interface/current_projects.php';
   
    
}
?>
