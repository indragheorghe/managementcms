<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="../CSS/Sources/open_page.css" type="text/css" />
        <script src="../js/jquery.js"></script>
        <script src="../js/functions_public.js"></script>
        <?php
        $page = "home";
        if (isset($_GET['page']))
            $page = $_GET['page'];
        ?>
        <script>
            $(document).ready(function() {
                var page= <?php echo json_encode($page); ?>;
                if(page=='home')
                    getActiveProjects();
                else
                    getPastProjects();
            });
        </script>
        <link rel="stylesheet" href="../CSS/Sources/active_projects.css" type="text/css" />
    </head>
    <body>
        <div class="container">
            <div class="left">
                <div class="menu">
                    <ul>
                        <li class="home"><a href="../interface/project_list_oid.php">Projects</a></li>
                        <li class="past_prj"><a href="../interface/project_list_oid.php?page=past_prj">Past Projects</a></li>
                        <li class="login"><a href="../login.php">Log In</a></li>
                        <li class="signup"><a id='username' href="../register.php">Sign Up</a></li>
                        <div class="clearFloat"></div>
                    </ul>
                </div>
            </div>
            <div class="right">
                <div class="active_projects">

                </div>
            </div>
    </body>
</html>
