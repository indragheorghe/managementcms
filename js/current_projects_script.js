$(document).ready(function() {
    getCurrentProjects();
});

function getCurrentProjects()
{
    var data=$.ajax({
        type: "POST",
        url: './services/projectdata.php',
        data: {
            data:"current_projects"
        }, 
        async: false,
        dataType: 'json'
    });
    var msg= data.responseText;
    msg=jQuery.parseJSON(msg);
       
    var template= $('.active_projects');
    var project_data;
    $.each( msg, function( key, value ) {
        project_data="<div id=project"+key+" class='project_title'>"+value.name+
        "</div><div class='line'></div><span class='label'>a</span><div class='tag_line'>"+value.tagline+
        "</div><span class='large_label'>Description</span><div class='description_line'>"+value.description+
        "</div><span class='large_label'>Start date</span><div class='start_date'>"+value.start_date+
        "</div><span class='large_label'>End date</span><div class='end_date'>"+value.end_date+"</div><div class='line'></div>";
        template.append(project_data);
    });
    // de adagat id-ul proiectului pentru a putea face click
}
