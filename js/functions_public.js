function getActiveProjects()
{
    var data=$.ajax({
        type: "POST",
        url: '../services/public_projectdata.php',
        data: {
            data:"active_project"
        }, 
        async: false,
        dataType: 'json'
    });
    var msg= data.responseText;
    msg=jQuery.parseJSON(msg);
    var template= $('.active_projects');
    var project_data;
    $.each( msg, function( key, value ) {
        project_data="<input type='button' id='"+value.id+"' class='project_title' value='"+value.name+
        "' onclick='project_page("+value.id+")'/><div><div class='line'></div><span class='label'>a</span><div class='tag_line'>"+value.tagline+
        "</div><span class='large_label'>Description</span><div class='description_line'>"+value.description+
        "</div><span class='large_label'>Start date</span><div class='start_date'>"+value.start_date+
        "</div><span class='large_label'>End date</span><div class='end_date'>"+value.end_date+"</div><div class='line'></div>";
        template.append(project_data);
    });
}

function getPastProjects()
{
    var data=$.ajax({
        type: "POST",
        url: '../services/public_projectdata.php',
        data: {
            data:"past_project"
        }, 
        async: false,
        dataType: 'json'
    });
    var msg= data.responseText;
    msg=jQuery.parseJSON(msg);
    var template= $('.active_projects');
    var project_data;
    $.each( msg, function( key, value ) {
        project_data="<input type='button' id='"+value.id+"' class='project_title' value='"+value.name+
        "' onclick='project_page("+value.id+")'/><div><div class='line'></div><span class='label'>a</span><div class='tag_line'>"+value.tagline+
        "</div><span class='large_label'>Description</span><div class='description_line'>"+value.description+
        "</div><span class='large_label'>Start date</span><div class='start_date'>"+value.start_date+
        "</div><span class='large_label'>End date</span><div class='end_date'>"+value.end_date+"</div><div class='line'></div>";
        template.append(project_data);
    });
    
    if(msg=='')
    {
        project_data='There are no past projects.';
        template.append(project_data);
    }
}
function project_page()
{
    // redirect to project_page
}