$(document).ready(function() {
   getUserProfileData();
   
});

function getUserProfileData()
{
    var data=$.ajax({
        type: "POST",
        url: './services/userdata.php',
        data: {
            data:"all_user_data"
        }, 
        async: false,
        dataType: 'json'
    });
    var msg= data.responseText;
    msg= jQuery.parseJSON(msg); // nu returneaza un obiect. trebuie reparsat si reformatat
    
    $('#fname').val(msg[0]);
    $('#lname').val(msg[1]);
    $('#email').val(msg[2]);
    $('#usrn').val(msg[3]);
    $('#cellphone').val(msg[4]);
    $('#birthday').val(msg[5]);
}



