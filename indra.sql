-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 07, 2013 at 07:17 PM
-- Server version: 5.5.27
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `indra`
--

-- --------------------------------------------------------

--
-- Table structure for table `budget`
--

CREATE TABLE IF NOT EXISTS `budget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project` int(11) NOT NULL,
  `product` varchar(70) NOT NULL,
  `sum` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project` (`project`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `budget_sponsor`
--

CREATE TABLE IF NOT EXISTS `budget_sponsor` (
  `budget_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  KEY `budget_id` (`budget_id`,`company_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `id_3` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `type`) VALUES
(1, 'admin'),
(2, 'universitate'),
(3, 'companie'),
(4, 'student'),
(5, 'profesor');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `contact` int(10) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(40) NOT NULL,
  `description` longtext,
  PRIMARY KEY (`id`),
  KEY `type` (`contact`),
  KEY `contact` (`contact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tel1` varchar(40) NOT NULL,
  `tel2` varchar(40) NOT NULL,
  `adress` varchar(100) NOT NULL,
  `website` varchar(40) NOT NULL,
  `blog` varchar(40) NOT NULL,
  `contact_person` int(11) NOT NULL,
  `main_email` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_person` (`contact_person`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(2, '1372938427'),
(2, '1372938498');

-- --------------------------------------------------------

--
-- Table structure for table `milestone`
--

CREATE TABLE IF NOT EXISTS `milestone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(3000) DEFAULT NULL,
  `tagline` varchar(200) DEFAULT NULL,
  `admin` int(10) DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL COMMENT '1',
  `project_type` int(10) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_signup_date` date DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `admin` (`admin`,`project_type`),
  KEY `created_by` (`created_by`),
  KEY `project_type` (`project_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `name`, `description`, `tagline`, `admin`, `created_by`, `project_type`, `start_date`, `end_date`, `creation_date`, `last_signup_date`, `private`) VALUES
(1, 'Realizarea unui robot de aspirare', 'In cadrul proiectului s eva realiza un robot pentru aspirat covoare cu senzori optici pentru aspirarea automata', 'Aspirarea a devenit mai usoara', 1, 1, 3, '2013-08-01', '2014-01-16', '2013-06-20 21:00:00', '2013-08-13', 0),
(2, 'Aplicatie android pentru controlul unui aspirator', 'Realizarea unei aplicatii android pentru controlul robotuli realizat in cadrul proiectului  "Realizarea unui robot de aspirare"', 'Aspira casa de la serviciu', 1, 2, 2, '2013-07-03', '2013-12-28', '2013-06-22 15:20:25', '2013-07-11', 0),
(13, 'Test', 'test', 'test', 1, 1, NULL, '2013-07-26', '2017-07-21', '2013-07-06 18:01:17', '2014-07-24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_description`
--

CREATE TABLE IF NOT EXISTS `project_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `description` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `project_description`
--

INSERT INTO `project_description` (`id`, `project_id`, `description`) VALUES
(1, 1, 'Un robot este este un operator mecanic sau virtual, artificial. Robotul este un sistem compus din mai multe elemente: mecanică, senzori și actuatori precum și un mecanism de direcționare. Mecanica stabilește înfățișarea robotului și mișcările posibile pe timp de funcționare. Senzorii și actorii sunt întrebuințați la interacția cu mediul sistemului. Mecanismul de direcționare are grijă ca robotul să-și îndeplinească obiectivul cu succes, evaluând de exemplu informațiile senzorilor. Acest mecanism reglează motoarele și planifică mișcările care trebuiesc efectuate.\r\nTot „robot”, prescurtat „bot”, pot fi numite programe (software) de calculator care îndeplinesc automat anumite funcții sau operațiuni. Astfel de roboți sunt virtuali, și nu mecanici.\r\nSensul cuvântului s-a schimbat de-alungul timpului. Termenul robot (din cehă robot) a fost utilizat de Josef Čapek și Karel Čapek în lucrările lor de science fiction la începutul secolului 20. Cuvântul robot este de origine slavă și se poate traduce prin: muncă, clacă sau muncă silnică. Karel Čapek a descris în piesa sa R.U.R. din anul 1921 muncitori de asemănare umană, care sunt crescuți în rezervoare. Čapek folosește în lucrarea sa motivele clasice de golem. Denumirea de astăzi a creaturilor lui Čapek este de android. Înaintea apariției termenului de robot s-au utilizat de expemplu în uzinele lui Stanisław Lem termenii automat și semiautomat.\r\nBazele roboților de azi stau mult mai departe. Primele modele de mașini pot fi mai degrabă numite automate (provenind din grecescul automatos, care se mișcă singur). Acestea nu puteau executa decât câte un singur obiectiv, fiind constrânse de construcție.\r\nMatematicianul grec Archytas a construit, conform unor relatări, unul dintre aceste prime automate: un porumbel propulsat cu vapori, care putea zbura singur. Acest porumbel cavernos din lemn era umplut cu aer sub presiune. Acesta avea un ventil care permitea deschiderea și închiderea printr-o contragreutate. Au urmat multe modele de'),
(2, 2, 'proiect 2'),
(4, 13, 'ceva');

-- --------------------------------------------------------

--
-- Table structure for table `project_files`
--

CREATE TABLE IF NOT EXISTS `project_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(70) NOT NULL,
  `name` varchar(70) NOT NULL,
  `project_id` int(11) NOT NULL,
  `uploader` int(11) DEFAULT NULL,
  `uploaded_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `size` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`project_id`),
  KEY `project_id` (`project_id`),
  KEY `uploader` (`uploader`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `project_files`
--

INSERT INTO `project_files` (`id`, `location`, `name`, `project_id`, `uploader`, `uploaded_date`, `size`) VALUES
(13, '../uploads/_814075237.jpg', 'Chevrolet_Camaro.jpg', 13, 1, '2013-07-07 13:00:53', 0),
(14, '../uploads/_250918316.pdf', 'Calendar.pdf', 2, 1, '2013-07-07 17:55:22', 0),
(15, '../uploads/_54564298.pdf', 'cms standards.pdf', 2, 1, '2013-07-07 17:55:44', 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_task`
--

CREATE TABLE IF NOT EXISTS `project_task` (
  `project_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  KEY `project_id` (`project_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_type`
--

CREATE TABLE IF NOT EXISTS `project_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `project_type`
--

INSERT INTO `project_type` (`id`, `type`) VALUES
(1, 'voluntariat'),
(2, 'finantat de stat'),
(3, 'finantat de companie'),
(4, 'finantat din eforturi de grup');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(40) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_daye` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `task_milestone`
--

CREATE TABLE IF NOT EXISTS `task_milestone` (
  `task_id` int(11) NOT NULL,
  `milestone_id` int(11) NOT NULL,
  KEY `task_id` (`task_id`,`milestone_id`),
  KEY `milestone_id` (`milestone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `task_user`
--

CREATE TABLE IF NOT EXISTS `task_user` (
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(70) NOT NULL,
  `last_name` varchar(70) NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(80) NOT NULL,
  `username` varchar(80) NOT NULL,
  `password` char(128) NOT NULL,
  `cellphone` varchar(30) DEFAULT NULL,
  `category_id` int(10) NOT NULL,
  `salt` char(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `birthday`, `email`, `username`, `password`, `cellphone`, `category_id`, `salt`) VALUES
(1, 'indra', 'gheorghe', '1991-01-30', 'indranatalia@gmail.com', 'indranatalia@gmail.com', '28a59c2ee932aaae35383b3b6178b4ed71c4c2fd33f8e0846cd1ef9b7d2bf12ab969d32ab94cc18100bb147988ab9dc6592e58f1e95afbcdb172ac411d702444', NULL, 1, NULL),
(2, 'Ion', 'Popa', '1977-06-11', 'ionpopa@mail.com', 'ionpopa@mail.com', '28a59c2ee932aaae35383b3b6178b4ed71c4c2fd33f8e0846cd1ef9b7d2bf12ab969d32ab94cc18100bb147988ab9dc6592e58f1e95afbcdb172ac411d702444', NULL, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_company`
--

CREATE TABLE IF NOT EXISTS `user_company` (
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_project`
--

CREATE TABLE IF NOT EXISTS `user_project` (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `project_id` (`project_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_project`
--

INSERT INTO `user_project` (`project_id`, `user_id`) VALUES
(1, 1),
(1, 2),
(2, 2),
(13, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `budget`
--
ALTER TABLE `budget`
  ADD CONSTRAINT `budget_ibfk_1` FOREIGN KEY (`project`) REFERENCES `project` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `budget_sponsor`
--
ALTER TABLE `budget_sponsor`
  ADD CONSTRAINT `budget_sponsor_ibfk_1` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `budget_sponsor_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `company_ibfk_1` FOREIGN KEY (`contact`) REFERENCES `contact` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `contact`
--
ALTER TABLE `contact`
  ADD CONSTRAINT `contact_ibfk_1` FOREIGN KEY (`contact_person`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `project_ibfk_3` FOREIGN KEY (`project_type`) REFERENCES `project_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `project_ibfk_4` FOREIGN KEY (`admin`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `project_description`
--
ALTER TABLE `project_description`
  ADD CONSTRAINT `project_description_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project_files`
--
ALTER TABLE `project_files`
  ADD CONSTRAINT `project_files_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `project_task`
--
ALTER TABLE `project_task`
  ADD CONSTRAINT `project_task_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_task_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `task_milestone`
--
ALTER TABLE `task_milestone`
  ADD CONSTRAINT `task_milestone_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `task_milestone_ibfk_2` FOREIGN KEY (`milestone_id`) REFERENCES `milestone` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `task_user`
--
ALTER TABLE `task_user`
  ADD CONSTRAINT `task_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `task_user_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_company`
--
ALTER TABLE `user_company`
  ADD CONSTRAINT `user_company_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_company_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_project`
--
ALTER TABLE `user_project`
  ADD CONSTRAINT `user_project_ibfk_3` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_project_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
